$(document).ready(function(){
    $("header").load("/admin/admin-header.html",_hamburger);
    $("footer").load("/footer.html");
    
    function _hamburger() {
        $(".hamburger").click(function(){
            $("header nav").slideToggle(250);
        })
    }
})