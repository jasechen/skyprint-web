$(document).ready(init);

function init() {
    
    var url = window.api
        
        var _session = window.localStorage.getItem("session");
        if ( !_session) {
            _doSessionInit();
        }else{
            $.get( url+"session/" + _session )
                .done(function (_response,stu,jqXHR) {
                    if( jqXHR.status !== 200 || jqXHR.status !== 201 ){
                        _doSessionInit();
                    }else{
                        if ( _response.status === "valid" ) {
                            _onSessionSuccess();
                        } else {
                            _doSessionInit();
                        }
                    }

                });
        }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData();
    }
    
    function _locationData() {
        _webPageIndex()
        _webPageUpload()
        _webPageLoc()
        _webPageAbout()
        _webPageCampus()
    };
    
    
    function _webPageIndex() {
        $.ajax( url + "web_content/list/asc/-1/index" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                $(_response).each(function(){
                    var $this = this.data.web_contents,
                        nowCover = $this[0].fileLinks[1].split("/")[1],
                        article_1_title = $this[1].title,
                        article_1_subtitle = $this[1].subtitle,
                        article_1_text = $this[1].text,
                        article_1_img = $this[1].fileLinks[1],

                        article_2_title = $this[2].title,
                        article_2_subtitle = $this[2].subtitle,
                        article_2_text = $this[2].text,
                        article_2_img = $this[2].fileLinks[1]
                    $(".c-index .up-cover .now-cover").text(nowCover)
                    $(".edit-a1 .edit-title input").val(article_1_title)
                    $(".edit-a1 .edit-sub-title input").val(article_1_subtitle)
                    $(".edit-a1 .now-cover").text(article_1_img)
                    $(".edit-a1 textarea").val(article_1_text)

                    $(".edit-a2 .edit-title input").val(article_2_title)
                    $(".edit-a2 .edit-sub-title input").val(article_2_subtitle)
                    $(".edit-a2 .now-cover").text(article_2_img)
                    $(".edit-a2 textarea").val(article_2_text)
                });
            }
        });
        
        
        
        $(".c-index .buttons .send-data").click(function() {
            var coverFile = $(".edit-cover .up-cover input")[0].files[0]
            var _coverFormData = new FormData();
                _coverFormData.append("file", coverFile);
                _coverFormData.append("_method", "PUT");
            
            var a1File = $(".edit-a1 .up-img input")[0].files[0]
            var _a1FormData = new FormData();
                _a1FormData.append("file", a1File);
                _a1FormData.append("_method", "PUT");
            
            var a2File = $(".edit-a2 .up-img input")[0].files[0]
            var _a2FormData = new FormData();
                _a2FormData.append("file", a2File);
                _a2FormData.append("_method", "PUT");
            
            
            $.ajax( url + "web_content/2" , {
                headers: { "Session": _session },
                method: "PUT",
                dataType:"json",
                data: {
                        "title": $(".edit-a1 .edit-title input").val(),
                        "subtitle": $(".edit-a1 .edit-sub-title input").val(),
                        "text": $(".edit-a1 textarea").val()
                    },
                success: function (_response) {
                }
            });
            
            if($(".c-index .edit-cover .up-cover input").val() != ""){
                $.ajax( url + "web_content_file/1" , {
                    headers: { "Session": _session },
                        method: "POST",
                        dataType:"json",
                        data: _coverFormData,
                        xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogress && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogress, false);
                                return xhr;
                            }
                        },
                        contentType: false,
                        processData: false,
                        success: function (_response) {

                        }
                });   
            }
            
            function onprogress(evt) {
            
                var loaded = evt.loaded; 
                var tot = evt.total; 
                var per = Math.floor(100 * loaded / tot); 
                
                if (per >= 0) {
                    $('.progress').show();
                }
                if (per == 100) {
                    $(".now-status").text("已儲存設定。")
                    $(".pop-up .edit-web button").removeAttr("disabled")
                    $(".pop-up .edit-web button").removeClass("isDisabled")
                }else{
                    $(".now-status").text("檔案上傳中。") 
                    $(".pop-up .edit-web button").attr("disabled" , "disabled").addClass("isDisabled")
                }
                
                $(".cover-progress").show().css("width", per + "%").find("span").html("cover 上傳中 ： " + per + "%");

            }
            
            if($(".edit-a1 .up-img input").val() != ""){
                $.ajax( url + "web_content_file/2" , {
                    headers: { "Session": _session },
                    method: "POST",
                    dataType:"json",
                    data: _a1FormData,
                    xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogress2 && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogress2, false);
                                return xhr;
                            }
                    },
                    contentType: false,
                    processData: false,
                    success: function (_response) {
                        
                    }
                });
            }
            
            function onprogress2(evt) {

                var loaded = evt.loaded; 
                var tot = evt.total; 
                var per = Math.floor(100 * loaded / tot); 
                
                if (per >= 0) {
                    $('.progress').show();
                }
                if (per == 100) {
                    $(".now-status").text("已儲存設定。")
                    $(".pop-up .edit-web button").removeAttr("disabled")
                    $(".pop-up .edit-web button").removeClass("isDisabled")
                }else{
                    $(".now-status").text("檔案上傳中。")
                    $(".pop-up .edit-web button").attr("disabled" , "disabled").addClass("isDisabled")
                }
                
                $(".a1-progress").show().css("width", per + "%").find("span").html("圖片1 上傳中 ： " + per + "%");
            }
            
            if($(".edit-a2 .up-img input").val() != ""){
                $.ajax( url + "web_content_file/3" , {
                    headers: { "Session": _session },
                    method: "POST",
                    dataType:"json",
                    data: _a2FormData,
                    xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogress3 && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogress3, false);
                                return xhr;
                            }
                    },
                    contentType: false,
                    processData: false,
                    success: function (_response) {
                        
                    }
                });
            }
            
            function onprogress3(evt) {

                var loaded = evt.loaded; 
                var tot = evt.total; 
                var per = Math.floor(100 * loaded / tot); 
                
                if (per >= 0) {
                    $('.progress').show();
                }
                if (per == 100) {
                    $(".now-status").text("已儲存設定。")
                    $(".pop-up .edit-web button").removeAttr("disabled")
                    $(".pop-up .edit-web button").removeClass("isDisabled")
                }else{
                    $(".now-status").text("檔案上傳中。") 
                    $(".pop-up .edit-web button").attr("disabled" , "disabled").addClass("isDisabled")
                }
                
                $(".a2-progress").show().css("width", per + "%").find("span").html("圖片2 上傳中 ： " + per + "%");

            }
            
            $.ajax( url + "web_content/3" , {
                headers: { "Session": _session },
                    method: "PUT",
                    dataType:"json",
                    data: {
                            "title": $(".edit-a2 .edit-title input").val(),
                            "subtitle": $(".edit-a2 .edit-sub-title input").val(),
                            "text": $(".edit-a2 textarea").val()
                        },
                    success: function (_response) {
                        
                    }
            }); 
            
            $(".pop-up").show()
             
        })
    }
    
    function _webPageUpload() {
        
        $.ajax( url + "web_content/list/asc/-1/line_upload_file" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
                dataType:"json",
                success: function (_response) {
                    $(_response).each(function(){
                        var $this = this.data.web_contents,
                            step1 = $this[0].text,
                            step2 = $this[1].text,
                            step3 = $this[2].text
                        $(".c-upload .edit-s1 textarea").val(step1)
                        $(".c-upload .edit-s2 textarea").val(step2)
                        $(".c-upload .edit-s3 textarea").val(step3)
                    });
                }
            });
        
        $(".c-upload .buttons .send-data").click(function() {
            
            $(".pop-up").show()
            
            $.ajax( url + "web_content/5" , {
                headers: { "Session": _session },
                    method: "PUT",
                    dataType:"json",
                    data: {
                            "text": $(".edit-s1 textarea").val()
                        },
                    success: function (_response) {
                        
                    }
            });
            

            $.ajax( url + "web_content/6" , {
                headers: { "Session": _session },
                    method: "PUT",
                    dataType:"json",
                    data: {
                            "text": $(".edit-s2 textarea").val()
                        },
                    success: function (_response) {

                    }
            });
            
            $.ajax( url + "web_content/7" , {
                headers: { "Session": _session },
                    method: "PUT",
                    dataType:"json",
                    data: {
                            "text": $(".edit-s3 textarea").val()
                        },
                    success: function (_response) {

                    }
            });
            
        });
        
    }
    
    function _webPageLoc() {
        
        $(".c-loc .buttons .send-data").click(function() {
            var locFile = $(".c-loc .up-cover input")[0].files[0]
            var _locCoverFormData = new FormData();
                _locCoverFormData.append("file", locFile);
                _locCoverFormData.append("_method", "PUT");
        
            if($(".c-loc .up-img input").val() != ""){
                $.ajax( url + "web_content_file/4" , {
                    headers: { "Session": _session },
                    method: "POST",
                    dataType:"json",
                    data: _locCoverFormData,
                    xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogressLoc && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogressLoc, false);
                                return xhr;
                            }
                    },
                    contentType: false,
                    processData: false,
                    success: function (_response) {
                        
                    }
                });
            }
            
            function onprogressLoc(evt) {

                var loaded = evt.loaded; 
                var tot = evt.total; 
                var per = Math.floor(100 * loaded / tot); 
                
                if (per >= 0) {
                    $('.progress').show();
                }
                if (per == 100) {
                    $(".now-status").text("已儲存設定。")
                    $(".pop-up .edit-web button").removeAttr("disabled")
                    $(".pop-up .edit-web button").removeClass("isDisabled")
                }else{
                    $(".now-status").text("檔案上傳中。")
                    $(".pop-up .edit-web button").attr("disabled" , "disabled").addClass("isDisabled")
                }
                
                $(".a1-progress").show().css("width", per + "%").find("span").html("圖片上傳中 ： " + per + "%");
            }
            $(".pop-up").show()
        })
        
            
        
        $.ajax( url + "web_content/list/asc/-1/machines" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
                dataType:"json",
                success: function (_response) {
                    $(_response).each(function(){
                        var $this = this.data.web_contents,
                            nowCover = $this[0].fileLinks[1].split("/")[1]
                        $(".c-loc .now-cover").text(nowCover)
                    });
                }
            });
    }
    
    function _webPageAbout() {
        
        
        $(".c-about .buttons .send-data").click(function() {
            
            $.ajax( url + "web_content/10" , {
                headers: { "Session": _session },
                    method: "PUT",
                    dataType:"json",
                    data: {
                            "text": $(".edit-about textarea").val()
                        },
                    success: function (_response) {
                        
                    }
            });
            /*
            var aboutFile = $(".c-about .up-cover input")[0].files[0]
            var _aboutCoverFormData = new FormData();
                _aboutCoverFormData.append("file", aboutFile);
                _aboutCoverFormData.append("_method", "PUT");
        
            if($(".c-about .up-img input").val() != ""){
                $.ajax( url + "web_content_file/5" , {
                    headers: { "Session": _session },
                    method: "POST",
                    dataType:"json",
                    data: _aboutCoverFormData,
                    xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogressAbout && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogressAbout, false);
                                return xhr;
                            }
                    },
                    contentType: false,
                    processData: false,
                    success: function (_response) {
                        
                    }
                });
            }
            
            function onprogressAbout(evt) {

                var loaded = evt.loaded; 
                var tot = evt.total; 
                var per = Math.floor(100 * loaded / tot); 
                
                if (per >= 0) {
                    $('.progress').show();
                }
                if (per == 100) {
                    $(".now-status").text("已儲存設定。")
                    $(".pop-up .edit-web button").removeAttr("disabled")
                    $(".pop-up .edit-web button").removeClass("isDisabled")
                }else{
                    $(".now-status").text("檔案上傳中。")
                    $(".pop-up .edit-web button").attr("disabled" , "disabled").addClass("isDisabled")
                }
                
                $(".a1-progress").show().css("width", per + "%").find("span").html("圖片上傳中 ： " + per + "%");
            }
            
            */
            $(".pop-up").show()
        })
        
        
        $.ajax( url + "web_content/list/asc/-1/about" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
                dataType:"json",
                success: function (_response) {
                    $(_response).each(function(){
                        var $this = this.data.web_contents,
                            nowCover = $this[0].fileLinks[1].split("/")[1],
                            nowText = $this[1].text
                        $(".c-about .now-cover").text(nowCover)
                        $(".c-about textarea").text(nowText)
                    });
                }
            });
        
    }
    
    function _webPageCampus() {
        
        $(".c-campus .buttons .send-data").click(function() {
            
            $.ajax( url + "web_content/12" , {
                headers: { "Session": _session },
                    method: "PUT",
                    dataType:"json",
                    data: {
                            "text": $(".edit-school textarea").val()
                        },
                    success: function (_response) {
                        
                    }
            });
            /*
            var campusFile = $(".c-campus .up-cover input")[0].files[0]
            var _campusCoverFormData = new FormData();
                _campusCoverFormData.append("file", campusFile);
                _campusCoverFormData.append("_method", "PUT");
        
            if($(".c-campus .up-img input").val() != ""){
                $.ajax( url + "web_content_file/6" , {
                    headers: { "Session": _session },
                    method: "POST",
                    dataType:"json",
                    data: _campusCoverFormData,
                    xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogressCampus && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogressCampus, false);
                                return xhr;
                            }
                    },
                    contentType: false,
                    processData: false,
                    success: function (_response) {
                        
                    }
                });
            }
            
            function onprogressCampus(evt) {

                var loaded = evt.loaded; 
                var tot = evt.total; 
                var per = Math.floor(100 * loaded / tot); 
                
                if (per >= 0) {
                    $('.progress').show();
                }
                if (per == 100) {
                    $(".now-status").text("已儲存設定。")
                    $(".pop-up .edit-web button").removeAttr("disabled")
                    $(".pop-up .edit-web button").removeClass("isDisabled")
                }else{
                    $(".now-status").text("檔案上傳中。")
                    $(".pop-up .edit-web button").attr("disabled" , "disabled").addClass("isDisabled")
                }
                
                $(".a1-progress").show().css("width", per + "%").find("span").html("圖片上傳中 ： " + per + "%");
            }
            */
            $(".pop-up").show()
        })
        
        $.ajax( url + "web_content/list/asc/-1/school" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
                dataType:"json",
                success: function (_response) {
                    $(_response).each(function(){
                        var $this = this.data.web_contents,
                            nowCover = $this[0].fileLinks[1].split("/")[1],
                            nowText = $this[1].text
                        $(".c-campus .now-cover").text(nowCover)
                        $(".c-campus textarea").text(nowText)
                    });
                }
            });
        
    }
    
}