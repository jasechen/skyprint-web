$(document).ready(init);

function init() {
            
    $(".change-page-btns nav a").click(_changeWebPage)
}

function _changeWebPage() {
    var $this = $(this),
        thisClass = $this.attr("class").split(" "),
        allButton = $(".change-page-btns nav a"),
        changeBox = $(".c-page")
    allButton.removeClass("now")
    $(".edit-web-page").find("li").removeClass("now")
    $(".edit-web-page").find("."+thisClass).addClass("now")
    $this.addClass("now")
}