$(document).ready(init);

function init() {
    var url = window.api
        
        var _session = window.localStorage.getItem("session");
        if ( !_session) {
            _doSessionInit();
        }else{
            $.get( url+"session/" + _session )
                .done(function (_response,stu,jqXHR) {

                    if( jqXHR.status !== 200 || jqXHR.status !== 201 ){
                        _doSessionInit();
                    }else{
                        if ( _response.status === "valid" ) {
                            _onSessionSuccess();
                        } else {
                            _doSessionInit();
                        }
                    }

                });
        }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData();
    }
    
    function _locationData() {
        _editLoc();
    };
    
    function _editLoc() {
        $.ajax( url + "machine/"+localStorage.pageLocId , {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                $(_response).each(function(){
                    var _data = this.data.machine,
                        _county = _data.county,
                        _zone = _data.zone,
                        _name = _data.name,
                        _short_name = _data.short_name,
                        _location_name = _data.location_name,
                        _location_note = _data.description,
                        _cover = _data.cover
                    window.cover = _cover
                    $(".county").val(_county);
                    $(".zone").val(_zone);
                    $(".name").val(_name);
                    $(".short-name").val(_short_name);
                    $(".location-name").val(_location_name);
                    $(".location-note").val(_location_note);
                    
                })
                _changeLocData();
            }
        });
        
    }
    
    function _changeLocData() {
        
        $(".location input").eq(0).focus();
        $(".send-change").hover(function(){
            
            $.ajax( url + "machine/"+localStorage.pageLocId  , {
                method: "GET",
                dataType:"JSON",
                headers: { "Session": _session },
                success: function (_response) {
                    $(".location-name").attr("old" , _response.data.machine.location_name)
                }
            });
        })
        
        $(".location .nld").keyup(function(){
            if( $(".location .nld").eq(0).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(1).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(2).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(3).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(4).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else{
                $(".change-btns .send-change").removeClass("isDisabled")
            }  
        })
        $(".location .nld").focusout(function(){
            
            if( $(".location .nld").eq(0).val() == "" ){
            $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(1).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(2).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(3).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(4).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else{
                $(".change-btns .send-change").removeClass("isDisabled")
                $(".change-btns .send-change").removeAttr("disabled")
            }
            
            var _this = $(this)
        
            if(_this.val() == ""){
                _this.addClass("error")
                _this.parents("li").find("span").show()
                _this.parents("li").find("span").text("請輸入資料")

            }else{
                _this.removeClass("error")
                _this.parents("li").find("span").hide()
                _this.parents("li").find("span").text("")         
            }
        })
        
        $(".send-change").click(function(){
            
            
            if( $(".location .nld").eq(0).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(1).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(2).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(3).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else if( $(".location .nld").eq(4).val() == "" ){
                $(".change-btns .send-change").addClass("isDisabled")
            }else{
                
            var file = $("input.n-location-img")[0].files[0]
                var _formData = new FormData();
                _formData.append("file", file);
                _formData.append("_method", "PUT");
            
            if($("input.n-location-img").val() != ""){
                $.ajax( url + "web_content_file/" + window.cover , {
                    headers: { "Session": _session },
                    method: "POST",
                    dataType:"json",
                    data: _formData,
                    xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogress && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogress, false);
                                return xhr;
                            }
                    },
                    contentType: false,
                    processData: false,
                    success: function (_response) {

                    }
                });
            }
            
            function onprogress(evt) {

                var loaded = evt.loaded; //已经上传大小情况
                var tot = evt.total; //附件总大小
                var per = Math.floor(100 * loaded / tot); //已经上传的百分比
                
                if (per >= 0) {
                    $('.progress').show();
                }
                if (per == 100) {
                    $(".now-status").text("已儲存設定。")
                    $(".pop-up .edit-web button").removeAttr("disabled")
                    $(".pop-up .edit-web button").removeClass("isDisabled")
                }else{
                    $(".now-status").text("檔案上傳中。") 
                    $(".pop-up .edit-web button").addClass("isDisabled")
                }
                
                $(".cover-progress").show().css("width", per + "%").find("span").html("圖片 上傳中 ： " + per + "%");

            }
            
            
            var _loc_id_number = $(".location").attr("value")

            var _cName = $(".zone").val(),
                _c_name = $(".name").val().replace(/\s+/g, ""),
                _cZone = $(".zone").val(),
                _cCounty = $(".county").val().replace(/\s+/g, ""),
                _cLocation_name = $(".location-name").val(),
                _cLocation_note = $(".location-note").val()
            
                if($(".location-name").attr("old") != $(".location-name").val()){
                
                    $.ajax( url + "machine/"+localStorage.pageLocId  , {
                        method: "PUT",
                        dataType:"JSON",
                        headers: { "Session": _session },
                        data: {
                                "name": _c_name,
                                "zone": _cZone,
                                "county": _cCounty,
                                "location_name": _cLocation_name,
                                "description": _cLocation_note
                            },
                        success: function (_response) {
                            $(".pop-up").show();
                        }
                    });

                }else{
                   
                    $.ajax( url + "machine/"+localStorage.pageLocId  , {
                        method: "PUT",
                        dataType:"JSON",
                        headers: { "Session": _session },
                        data: {
                                "name": _c_name,
                                "zone": _cZone,
                                "county": _cCounty,
                                "description": _cLocation_note
                            },
                        success: function (_response) {
                            $(".pop-up").show();
                        }
                    });

                }
            }
        })
        
    };

}