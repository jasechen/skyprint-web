$(document).ready(init);

function init() {
    var url = window.api
        
        var _session = window.localStorage.getItem("session");
        if ( !_session) {
            _doSessionInit();
        }else{
            $.get( url+"session/" + _session )
                .done(function (_response,stu,jqXHR) {

                    if( jqXHR.status !== 200 || jqXHR.status !== 201 ){
                        _doSessionInit();
                    }else{
                        if ( _response.status === "valid" ) {
                            _onSessionSuccess();
                        } else {
                            _doSessionInit();
                        }
                    }

                });
        }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData();
    }
    
    function _locationData() {
        _faq();
    };
    
    function _faq() {
        
        $.ajax( url + "web_content/list/DESC/-1/faq/content" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                var $this = _response.data.web_contents,
                    _length = $this.length
                for ( var _quantity = 0 ; _quantity < _length ; _quantity ++ ) {
                    var _html = "<li>"
                            +"<div>"+ $this[_quantity].subtitle +"</div>"
                            +'<div>'
                            +'<button'+' data-page="'+ $this[_quantity].id +'"' +' type="button" class="change-btn">'+"<i class='fas fa-pencil-alt'></i> " +'修改</button>'
                            +'<button'+' data-delete="'+ $this[_quantity].id +'"' +' type="button" class="delete-btn">'+"<i class='far fa-trash-alt'></i> "+'刪除</button>'
                            +'</div>'
                            + "</li>"
                    $(".faq-list").append(_html)
                }
                $(".faq-list .change-btn").click(_editFaq)
                $(".faq-list .delete-btn").click(_deleteFaq)
                        

            }
        });
    }
    
    function _editFaq() {
        location.href='admin-edit-faq.html'
        localStorage.pageId = $(this).attr("data-page")
    }
    
    function _deleteFaq() {
        var _deteteID = $(this).attr("data-delete")
        $(".pop-up").show();
        
        $(".pop-up button").click(function(){
            
            if($(this).attr("class") == "yes"){
                
                $.ajax( url + "web_content/"+_deteteID , {
                    method: "DELETE",
                    headers: { "Session": _session },
                    dataType:"json",
                    data:{},
                    success: function (_response) {
                        location.reload(); 
                    }
                });
            }else{
                $(".pop-up").hide();
                _deteteID = "";
            }
            
        });
    }
    
}