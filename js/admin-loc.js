$(document).ready(init);

function init() {
    var url = window.api
        
        var _session = window.localStorage.getItem("session");
        if ( !_session) {
            _doSessionInit();
        }else{
            $.get( url+"session/" + _session )
                .done(function (_response,stu,jqXHR) {

                    if( jqXHR.status !== 200 || jqXHR.status !== 201 ){
                        _doSessionInit();
                    }else{
                        if ( _response.status === "valid" ) {
                            _onSessionSuccess();
                        } else {
                            _doSessionInit();
                        }
                    }

                });
        }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData();
    }
    
    function _locationData() {
        _loc();
    };
    
    function _loc() {
        $.ajax( url + "machine/list/DESC/-1" + "?v=" + (+new Date()), {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                
                var _data = _response.data.machines,
                    _length = _data.length                        
                for ( var _quantity = 0 ; _quantity < _length ; _quantity ++ ) {
                    var _thisData = _data[_quantity],
                        _county = _thisData.county,
                        _zone = _thisData.zone,
                        _name = _thisData.name,
                        _location_name = _thisData.location_name,
                        _location_name2 = String(_thisData.location_name).split("-")[0],
                        _id = _thisData.id

                    var _loc_html_count = "<div>"+_county+"</div>",
                        _loc_html_zone = "<div>"+_zone+"</div>",
                        _loc_html_short_name = "<div>"+_name+"</div>",
                        _loc_html_location_name = "<div>"+_location_name+"</div>",
                        _change_btn = "<button type='button'"+"data-page='"+_id+"' class='change-btn'>"+"<i class='fas fa-pencil-alt'></i> "+"修改"+"</button>",
                        _delete_btn = "<button type='button'"+"data-delete='"+_id+"' class='delete-btn'>"+"<i class='far fa-trash-alt'></i> "+"刪除"+"</button>",
                        _loc_html_btn = "<div>"+_change_btn+_delete_btn+"</div>"

                    var _loc_html = "<li>"
                                    +_loc_html_count 
                                    + _loc_html_zone 
                                    + _loc_html_short_name 
                                    + _loc_html_location_name 
                                    + _loc_html_btn
                                    +"</li>"

                    $(".loc-list").append(_loc_html)

            }
                
            $(".loc-list .change-btn").click(_changeLoc)
            $(".loc-list .delete-btn").click(_deleteLoc)
                
            }
        });
    }
    
    function _changeLoc() {
        location.href='admin-edit-location.html'
        localStorage.pageLocId = $(this).attr("data-page")
    }
    
    function _deleteLoc() {
        var _deteteID = $(this).attr("data-delete")
        $(".pop-up").show();
        
        $(".pop-up button").click(function(){
            
            if($(this).attr("class") == "yes"){
                
                $.ajax( url + "machine/"+_deteteID , {
                    method: "DELETE",
                    headers: { "Session": _session },
                    dataType:"json",
                    data:{},
                    success: function (_response) {
                        location.reload(); 
                    }
                });
            }else{
                $(".pop-up").hide();
                _deteteID = "";
            }
            
        });
    }
}