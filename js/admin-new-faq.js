$(document).ready(init);

function init() {
    var url = window.api
        
        var _session = window.localStorage.getItem("session");
        if ( !_session) {
            _doSessionInit();
        }else{
            $.get( url+"session/" + _session )
                .done(function (_response,stu,jqXHR) {

                    if( jqXHR.status !== 200 || jqXHR.status !== 201 ){
                        _doSessionInit();
                    }else{
                        if ( _response.status === "valid" ) {
                            _onSessionSuccess();
                        } else {
                            _doSessionInit();
                        }
                    }

                });
        }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData();
    }
    
    function _locationData() {
        _editFaq();
    };
    
    function _editFaq() {
        
        $(".nc-faq .cfaq").keyup(function(){
            if( $(".nc-faq .cfaq").eq(0).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
            }else if( $(".nc-faq .cfaq").eq(1).val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
            }else{
                $(".change-btns .send-btn").removeClass("isDisabled")
                $(".change-btns .send-btn").removeAttr("disabled")
            }
        })
        
        $(".nc-faq .cfaq").focusout(function(){
            
            if( $(".nc-faq .cfaq").eq(0).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
            }else if( $(".nc-faq .cfaq").eq(1).val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
            }else{
                $(".change-btns .send-btn").removeClass("isDisabled")
                $(".change-btns .send-btn").removeAttr("disabled")
            }
            
            var _this = $(this)
        
            if(_this.val() == ""){
                _this.addClass("error")
                _this.parents("li").find("span").show()
                _this.parents("li").find("span").text("請輸入資料")

            }else{
                _this.removeClass("error")
                _this.parents("li").find("span").hide()
                _this.parents("li").find("span").text("")         
            }
        })
        
        $(".send-btn").click(function(){
            
            if( $(".nc-faq .cfaq").eq(0).val() == "" ){
                
                $(".change-btns .send-btn").addClass("isDisabled")
                $(".nc-faq .cfaq").eq(0).addClass("error")
                $(".nc-faq .cfaq").eq(0).parents("li").find("span").show()
                $(".nc-faq .cfaq").eq(0).parents("li").find("span").text("請輸入資料")
                
            }else if( $(".nc-faq .cfaq").eq(1).val() == "" ){
                
                $(".change-btns .send-btn").addClass("isDisabled")
                $(".nc-faq .cfaq").eq(1).addClass("error")
                $(".nc-faq .cfaq").eq(1).parents("li").find("span").show()
                $(".nc-faq .cfaq").eq(1).parents("li").find("span").text("請輸入資料")
                
            }else{
                
                $.ajax( url + "web_content" , {
                    headers: { "Session": _session },
                    method: "POST",
                    dataType:"json",
                    data: {
                            "subtitle": $(".question").val(),
                            "text": $(".answer").val(),
                            "file_id": "9999",
                            "sort": "1",
                            "type": "faq",
                            "subtype": "content"
                        },
                    success: function (_response) {
                        $(".pop-up").show()
                    }
                });   
            }
            
            $(".nc-faq .cfaq").each(function(){
                var _this = $(this),
                    _val = _this.val()

                if(_val == ""){
                    _this.addClass("error")
                    _this.parents("li").find("span").show();
                    _this.parents("li").find("span").text("請輸入資料")
                }
                
            });
            
        })
        
    }

}