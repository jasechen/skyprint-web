$(document).ready(init);

function init() {
    var url = window.api
        
        var _session = window.localStorage.getItem("session");
        if ( !_session) {
            _doSessionInit();
        }else{
            $.get( url+"session/" + _session )
                .done(function (_response,stu,jqXHR) {

                    if( jqXHR.status !== 200 || jqXHR.status !== 201 ){
                        _doSessionInit();
                    }else{
                        if ( _response.status === "valid" ) {
                            _onSessionSuccess();
                        } else {
                            _doSessionInit();
                        }
                    }

                });
        }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData();
    }
    
    function _locationData() {
        _newLoc();
    };
    
    
    function _newLoc() {
        
        
        $("input.n-location-img").change(function(){
                var file = $(this)[0].files[0]
                var _formData = new FormData();
                _formData.append("file", file);
                $.ajax( url + "web_content_file" , {
                    headers: { "Session": _session },
                    method: "POST",
                    dataType:"json",
                    data: _formData,
                    xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogress && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogress, false);
                                return xhr;
                            }
                    },
                    contentType: false,
                    processData: false,
                    success: function (_response) {
                        window.coverId = _response.data.file_id
                    }
                });
            
            function onprogress(evt) {

                var loaded = evt.loaded; //已经上传大小情况
                var tot = evt.total; //附件总大小
                var per = Math.floor(100 * loaded / tot); //已经上传的百分比
                
                if (per >= 0) {
                    $('.progress').show();
                }
                if (per == 100) {
                    $(".now-status").text("已儲存設定。")
                    $(".pop-up .edit-web button").removeAttr("disabled")
                    $(".pop-up .edit-web button").removeClass("isDisabled")
                }else{
                    $(".now-status").text("檔案上傳中。") 
                    $(".pop-up .edit-web button").addClass("isDisabled")
                }
                
                $(".cover-progress").show().css("width", per + "%").find("span").html("圖片 上傳中 ： " + per + "%");

            }
        })  
        
        
        
//        $(".location li").mouseenter(_focusInput2);
        $(".location .nld").focusout(_focusInput);
        $(".location .nld").keyup(_keyup)
        
        $(".send-btn").click(function(){
            
            var _data1 = $(".location .nld").eq(0),
                _data2 = $(".location .nld").eq(1),
                _data3 = $(".location .nld").eq(2),
                _data4 = $(".location .nld").eq(3),
                _data5 = $(".location .nld").eq(4),
                _data6 = $(".location .nld").eq(5),
                _data7 = $(".location .nld").eq(6)
            
            var _loc_code = $(".n-code").val().replace(/\s+/g, ""),
                _loc_name = $(".n-name").val().replace(/\s+/g, ""),
                _loc_short_name = $(".n-short-name").val(),
                _loc_zone = $(".n-zone").val(),
                _loc_county = $(".n-county").val().replace(/\s+/g, ""),
                _loc_location_name = $(".n-location-name").val(),
                _loc_forder_name = $(".n-code").val().replace(/\s+/g, ""),
                _loc_note = $(".location-note").val();
            
            if( _data1.val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
                _data1.addClass("error")
                _data1.parents("li").find("span").show();
                _data1.parents("li").find("span").text("請輸入資料")
            }else if( _data2.val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
                _data2.addClass("error")
                _data2.parents("li").find("span").show();
                _data2.parents("li").find("span").text("請輸入資料")
            }else if( _data3.val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
                _data3.addClass("error")
                _data3.parents("li").find("span").show();
                _data3.parents("li").find("span").text("請輸入資料")
            }else if( _data4.val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
                _data4.addClass("error")
                _data4.parents("li").find("span").show();
                _data4.parents("li").find("span").text("請輸入資料")
            }else if( _data5.val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
                _data5.addClass("error")
                _data5.parents("li").find("span").show();
                _data5.parents("li").find("span").text("請輸入資料")
            }else if( _data6.val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
                _data6.addClass("error")
                _data6.parents("li").find("span").show();
                _data6.parents("li").find("span").text("請輸入資料")
            }else if( _data7.val() == "" ){
                $(".change-btns .send-btn").addClass("isDisabled")
                _data7.addClass("error")
                _data7.parents("li").find("span").show();
                _data7.parents("li").find("span").text("請輸入資料")
            }else{
                $.ajax( url + "machine" , {
                    method: "POST",
                    dataType: "json",
                    headers: { "Session": _session },
                    data : {
                            "cover": window.coverId,
                            "name": _loc_name,
                            "zone": _loc_zone,
                            "county": _loc_county,
                            "location_name": _loc_location_name,
                            "status": "enable",
                            "forder_name": _loc_forder_name,
                            "code": _loc_code,
                            "description" : _loc_note
                            },
                    success: function (_response) {
                        $(".pop-up").show();
                    }
                }); 
            }
            
                   
            $(".location .nld").each(function(){
                var _this = $(this),
                    _val = _this.val()

                if(_val == ""){
                    _this.addClass("error")
                    _this.parents("li").find("span").show();
                    _this.parents("li").find("span").text("請輸入資料")
                }
                
            });
             
        })
        
    }
    
    function _keyup() {
        if( $(".location .nld").eq(0).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(1).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(2).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(3).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(4).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(5).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(6).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else{
            $(".change-btns .send-btn").removeClass("isDisabled")
            $(".change-btns .send-btn").removeAttr("disabled")
        }
    }
    
    function _focusInput() {
        
        var _this = $(this)
        
        if(_this.val() == ""){
            _this.addClass("error")
            _this.parents("li").find("span").show()
            _this.parents("li").find("span").text("請輸入資料")
            
        }else{
            _this.removeClass("error")
            _this.parents("li").find("span").hide()
            _this.parents("li").find("span").text("")
            $(".change-btns .send-btn").removeAttr("disabled")
                
        }
        
        if( $(".location .nld").eq(0).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(1).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(2).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(3).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(4).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(5).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else if( $(".location .nld").eq(6).val() == "" ){
            $(".change-btns .send-btn").addClass("isDisabled")
        }else{
            $(".change-btns .send-btn").removeClass("isDisabled")
            $(".change-btns .send-btn").removeAttr("disabled")
        }
        
//        if($(this).val()=="" || $(this).parents("li") ){
//           $(".change-btns .send-btn").addClass("isDisabled")
//        }else{
//            $(".change-btns .send-btn").removeAttr("disabled")
//            $(".change-btns .send-btn").removeClass("isDisabled")
//        }
             
        $.ajax( url + "machine/list/ASC/-1" , {
                method: "GET",
                dataType: "json",
                headers: { "Session": _session },
                success: function (_response) {
                    var _machines = _response.data.machines,
                        _length = _machines.length
                    
                    for ( var _quantity = 0 ; _quantity < _length ; _quantity ++ ) {
                        var _this =  _machines[_quantity],
                            _existingCode = _this.code,
                            _existingLocation_name = _this.location_name

                    if($(".n-code").val() ==_existingCode){
                       $(".n-code").addClass("error");
                        $(".n-code").parents("li").find("span").show();
                        $(".n-code").parents("li").find("span").text("已與現有的其他設備設定重複");
                        return;
                        
                    }else{
                        $(".n-code").removeClass("error");
                        $(".n-code").parents("li").find("span").hide();
                    };
                        
                    }  
                }
        });
        
    };
    
}