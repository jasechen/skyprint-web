$(document).ready(init);

function init () {
    
    var url = window.api
        
        var _session = window.localStorage.getItem("session");
        if ( !_session) {
            _doSessionInit();
        }else{
            $.get( url+"session/" + _session )
                .done(function (_response,stu,jqXHR) {

                    if( jqXHR.status !== 200 || jqXHR.status !== 201 ){
                        _doSessionInit();
                    }else{
                        if ( _response.status === "valid" ) {
                            _onSessionSuccess();
                        } else {
                            _doSessionInit();
                        }
                    }

                });
        }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData();
    }
    
    function _locationData() {
        _machine();
        $(".admin-box .plus").click(_plusLoc);
    };
    
    
    function _machine() {

         $.ajax( url + "machine/list/ASC/-1" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
                dataType:"json",
                success: function (_response) {
                    $(_response).each(function(){
                        var _data = this.data.machines,
                            _length = _data.length
                        var _last_id = this.data.machines[_length-1].id
                        for ( var _quantity = 0 ; _quantity < _length ; _quantity ++ ) {
                            var _thisData = _data[_quantity],
                                _county = _thisData.county,
                                _zone = _thisData.zone,
                                _name = _thisData.name,
                                _location_name = _thisData.location_name,
                                _location_name2 = String(_thisData.location_name).split("-")[0],
                                _id = _thisData.id
                            
                            var _loc_html_count = "<div>"+_county+"</div>",
                                _loc_html_zone = "<div>"+_zone+"</div>",
                                _loc_html_short_name = "<div>"+_name+"</div>",
                                _loc_html_location_name = "<div>"+_location_name+"</div>",
                                _change_btn = "<button type='button'"+"loc_id='"+_id+"' class='change-btn'>"+"<i class='fas fa-pencil-alt'></i> "+"修改"+"</button>",
                                _delete_btn = "<button type='button'"+"loc_id='"+_id+"' class='delete-btn'>"+"<i class='far fa-trash-alt'></i> "+"刪除"+"</button>",
                                _loc_html_btn = "<div>"+_change_btn+_delete_btn+"</div>"
                            
                            var _loc_html = "<li>"+_loc_html_count + _loc_html_zone + _loc_html_short_name + _loc_html_location_name + _loc_html_btn+"</li>"
                            
                            $(".loc-list").append(_loc_html)
                            
                        }
                        $(".loc-list button.change-btn").click(_changeLoc);
                        
                        $(".delete-btn").click(_deleteLoc);
                        
                    });
                }
            });
    };
    
    
    function _deleteLoc() {
        $(".pop-up").show();
        
        var _loc_id_number = $(this).attr("loc_id")
        $(".pop-up button").click(function(){
            if($(this).attr("class") == "yes"){
                
                $.ajax( url + "machine/"+_loc_id_number , {
                    method: "DELETE",
                    headers: { "Session": _session },
                    dataType:"json",
                    data:{},
                    success: function (_response) {
                        location.reload(); 
                    }
                });
            }else{
                $(".pop-up").hide();
                _loc_id_number = "";
            }
        })
    }
    
    $(".pop-up .close").click(closePopup);
    
    function closePopup() {
    $(".pop-up").hide();
    $("body").css("overflow","auto");
    $(".pop-up ul").eq(1).html("")
    $(".pop-up ul input").val("")
    };
    
    function _changeLoc() {
        $(".admin-box").load("/admin/template/admin-change-location.html" , _changeLocData )
        var _loc_id_number = $(this).attr("loc_id")

        $.ajax( url + "machine/"+_loc_id_number , {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                $(_response).each(function(){
                    var _data = this.data.machine,
                        _county = _data.county,
                        _zone = _data.zone,
                        _name = _data.name,
                        _short_name = _data.short_name,
                        _location_name = _data.location_name

                    $(".county").val(_county);
                    $(".zone").val(_zone);
                    $(".name").val(_name);
                    $(".short-name").val(_short_name);
                    $(".location-name").val(_location_name);
                    $(".location").attr("value",_loc_id_number)
                    
                })
            }
        });
          
    }
    

    
    function _plusLoc() {
        $(".admin-box").load("/admin/template/admin-new-location.html" , _sendUpNewLoc);
    }
    
    function _changeLocData() {        
        $(".location input").eq(0).focus();
        $(".send-change").hover(function(){
            var _loc_id_number = $(".location").attr("value")
            
            $.ajax( url + "machine/"+_loc_id_number , {
                method: "GET",
                dataType:"JSON",
                headers: { "Session": _session },
                success: function (_response) {
                    $(".location-name").attr("old" , _response.data.machine.location_name)
                }
            });
        })
        
        $(".location input").focusout(function(){
            
            var _this = $(this)
        
            if(_this.val() == ""){
                _this.addClass("error")
                _this.parents("li").find("span").show()
                _this.parents("li").find("span").text("請輸入資料")
                return true;
            }else{
                _this.removeClass("error")
                _this.parents("li").find("span").hide()
                _this.parents("li").find("span").text("")
                
                
                
        $(".send-change").click(function(){
            var _loc_id_number = $(".location").attr("value")

            var _cName = $(".zone").val(),
                _c_name = $(".name").val().replace(/\s+/g, ""),
                _cZone = $(".zone").val(),
                _cCounty = $(".county").val().replace(/\s+/g, ""),
                _cLocation_name = $(".location-name").val()
            
                if($(".location-name").attr("old") != $(".location-name").val()){
                
                    $.ajax( url + "machine/"+_loc_id_number , {
                        method: "PUT",
                        dataType:"JSON",
                        headers: { "Session": _session },
                        data: {
                                "name": _c_name,
                                "zone": _cZone,
                                "county": _cCounty,
                                "location_name": _cLocation_name
                            },
                        success: function (_response) {
                            location.reload();
                        }
                    });

                }else{
                   
                    $.ajax( url + "machine/"+_loc_id_number , {
                        method: "PUT",
                        dataType:"JSON",
                        headers: { "Session": _session },
                        data: {
                                "name": _c_name,
                                "zone": _cZone,
                                "county": _cCounty
                            },
                        success: function (_response) {
                            location.reload();
                        }
                    });

                }

            })
        
                
            }
            
        })
        
    };
    
    function _sendUpNewLoc() {
        
        $(".location input").focusout(_focusInput);
        $(".ln-code , .n-location-name").mouseleave(_focusInput);
        
        $(".send-btn").click(function(){
            
            $(".location input").each(function(){
            
            var _this = $(this),
                _val = _this.val()

            if(_val == ""){
                _this.addClass("error")
                _this.parents("li").find("span").show();
                _this.parents("li").find("span").text("請輸入資料")
                return;
            }

        });

        var _loc_code = $(".n-code").val().replace(/\s+/g, ""),
            _loc_name = $(".n-name").val().replace(/\s+/g, ""),
            _loc_short_name = $(".n-short-name").val(),
            _loc_zone = $(".n-zone").val(),
            _loc_county = $(".n-county").val().replace(/\s+/g, ""),
            _loc_location_name = $(".n-location-name").val(),
            _loc_forder_name = $(".n-code").val().replace(/\s+/g, "")
    
            $.ajax( url + "machine" , {
                    method: "POST",
                    dataType: "json",
                    headers: { "Session": _session },
                    data : {
                            "name": _loc_name,
                            "zone": _loc_zone,
                            "county": _loc_county,
                            "location_name": _loc_location_name,
                            "status": "enable",
                            "forder_name": _loc_forder_name,
                            "code": _loc_code
                            },
                    success: function (_response) {
                        location.reload();
                    }
            });  
        })
        
    }
    
    function _focusInput() {
        
        var _this = $(this)
        
        if(_this.val() == ""){
            _this.addClass("error")
            _this.parents("li").find("span").show()
            _this.parents("li").find("span").text("請輸入資料")
        }else{
            _this.removeClass("error")
            _this.parents("li").find("span").hide()
            _this.parents("li").find("span").text("")
        }
             
        $.ajax( url + "machine/list/ASC/-1" , {
                method: "GET",
                dataType: "json",
                headers: { "Session": _session },
                success: function (_response) {
                    var _machines = _response.data.machines,
                        _length = _machines.length
                    
                    for ( var _quantity = 0 ; _quantity < _length ; _quantity ++ ) {
                        var _this =  _machines[_quantity],
                            _existingCode = _this.code,
                            _existingLocation_name = _this.location_name

                    if($(".n-code").val() ==_existingCode){
                       $(".n-code").addClass("error");
                        $(".n-code").parents("li").find("span").show();
                        $(".n-code").parents("li").find("span").text("已與現有的其他設備設定重複");
                        return;
                        
                    }else{
                        $(".n-code").removeClass("error");
                        $(".n-code").parents("li").find("span").hide();
                    };
                        
                    }  
                }
        });
        
    };

};









