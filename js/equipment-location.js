$(document).ready(init);

function init () {
    
    
    var url = window.api

    var _session = window.localStorage.getItem("session");
    if ( !_session) {
        _doSessionInit();
    }else{
        $.get( url+"session/" + _session )
            .done(function (_response,stu,jqXHR) {

                if( jqXHR.status !== 200 || jqXHR.status !== 201){
                    _doSessionInit();
                }else{
                    if ( _response.status === "valid" ) {
                        _onSessionSuccess();
                    } else {
                        _doSessionInit();
                    }
                }

            });
    }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData()
    }
    

    
    
    function _locationData () {
        _machine();
    }

    
    
    
    function _machine() {
        
        
      $.ajax( url + "web_content/list/asc/-1/machines/banner" + "?v=" + (+new Date()) , {
            headers: {"Session": _session}, 
            dataType:"json",
            success: function (_response) {
                    var $this = _response.data.web_contents,
                        nowCover = $this[0].fileLinks[0]
                    $(".banner.loc img").attr("src",nowCover)
            }
      })
        
        $.ajax( url + "machine/list/ASC/-1" + "?v=" + (+new Date()) , {
                headers: {"Session": _session}, 
                dataType:"json",
                success: function (_response) {
                    $(_response).each(function(){
                        
                        var _city2 = "";
                        
                        var _data = this.data.machines,
                            _length = _data.length,
                            _alt = "alt"
                        
                        for ( var _quantity = 0 ; _quantity < _length ; _quantity ++ ) {
                            var _thisData = _data[_quantity],
                                _addressLength = _thisData.location_name.split("-").length-1,
                                _address = _thisData.location_name.split("-")[_addressLength],
                                _location = _thisData.name,
                                _img = _thisData.coverLinks[0],
                                _locImg = '<img src="'+_img+'" alt="'+ _alt +'">',
                                _note = _thisData.description,
                                _addHtml = "<div value='"+_thisData.county+"'>"
                                            + "<div class='loc-img'>"+_locImg+"</div>"
                                            + "<div class='place'>"+_location+"</div>"
                                            + "<div class='address'>"+_address+"</div>"
                                            + "<div class='loc-doc'>"+_note+"</div>"
                                            + "</div>"
                            $(".location-box").append(_addHtml)
                            
                            
                            var _city = _thisData.county

                            var cityButtons = "<span class='cityArray'>"+_city+"</span>",
                                array = $(".location-btn-group").text().trim().split(",")
                            $(".location-btn-group").append(cityButtons)
                        }
                        
                        var allCity = [ "新北市", "桃園市", "新竹縣", "台北市", "台中市", "台南市", "高雄市", "基隆市", "新竹市", "嘉義縣", "嘉義市", "苗栗縣", "彰化縣", "南投縣", "雲林縣", "屏東縣", "宜蘭縣", "花蓮縣", "台東縣", "澎湖縣", "金門縣", "連江縣" ]
                        
                        for (var city = 0 ; city < allCity.length ; city ++ ){
                            var _button = '<button type="button" disabled class="disabled">' + allCity[city] + '</button>'
                            $(".location-btn-group").append(_button)
                        }
                        $(".location-btn-group").append('<button type="button">查看全部</button>')
                        $(".location-btn-group button").each(function(){
                            var _this = $(this),
                                _text = _this.text()
                            for ( var _quantity = 0 ; _quantity < _length ; _quantity ++ ) {
                                var _thisData = _data[_quantity]
                                if(_text == _thisData.county){
                                    _this.removeAttr("disabled").removeClass("disabled")
                                }
                            }
                        })
                        
                        $(".location-btn-group button").click(citySelect);
                    })
                    }
            });
    }
    
    
};

function citySelect () {
    
    
    var _this = $(this)  
                
    $(".location-btn-group button").removeClass("now");
    _this.addClass("now");
    
    $(".location-box > div").each(function(){
        if($(this).attr("value") == _this.text()){
            $(this).show()
        } else if (_this.text() == "查看全部") {
            $(".location-box > div").show();
        }else{
            $(this).hide()
        }
    })
    
};
