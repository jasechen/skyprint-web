$(document).ready(init);

function init() {

    var url = window.api

    var _session = window.localStorage.getItem("session");
    if (!_session) {
        _doSessionInit();

    } else {
        $.get( url + "session/" + _session)
            .done(function (_response, stu, jqXHR) {

                if (jqXHR.status !== 200 || jqXHR.status !== 201) {
                    _doSessionInit();
                } else {
                    if (_response.status === "valid") {
                        _onSessionSuccess();
                    } else {
                        _doSessionInit();
                    }
                }

            });
    }

    function _doSessionInit() {
        $.post( url + "session/init", null, _onSessionSuccess);
    }

    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session", _session);
        _locationData()
    }

    function _locationData() {

        $(".inquire-btn").click(_checkOrders);

    };

    function _checkOrders() {

        var _phoneNumber = $(".phone-number").val()
        $(".inquire-box").html("")
        var _titleHtml = "<li></li>"
        $(".inquire-box").append(_titleHtml)

        $.ajax( url + "order/phone/" + _phoneNumber, {
            headers: {
                "Session": _session
            },
            dataType: "json",
            success: function (_response) {
                $(_response).each(function () {
                    var _order = this.data.order,
                        _length = _order.length
                    if (_order == "") {
                        $(".inquire-box").find("li").eq(0).text("查無資料")
                    } else {
                        $(".inquire-box").find("li").eq(0).text("檔案名稱")
                        for (var _quantity = 0; _quantity < _length; _quantity++) {
                            var _thisOrders = _order[_quantity],
                                _file_link = _thisOrders.orders,
                                _file_date = _thisOrders.updated_at,
                                _file_length = _file_link.length

                            for (var _fileQuantity = 0; _fileQuantity < _file_length; _fileQuantity++) {
                                var _file_link_dtat = _file_link[_fileQuantity].original_file_name,
                                    _note = _file_link[_fileQuantity].note,
                                    _time = _file_link[_fileQuantity].updated_at,
                                    _link_htm = "<div>"+_file_link_dtat+"</div>",
                                    _note_htm = "<div>"+_note+"</div>",
                                    _time_htm = "<div>"+_time+"</div>",
                                    _file_name = String(_file_link_dtat).replace("order/", ""),
                                    _flie_html = "<li>" + _time_htm + _link_htm + _note_htm + "</li>"

                                $(".inquire-box").append(_flie_html)

                            }

                        }
                    }


                });

            }
        });
    }

};