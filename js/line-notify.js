$(document).ready(init);
function init() {
    
    var url = window.api
    
    var _session = window.localStorage.getItem("session");
    if (!_session) {
        _doSessionInit();

    } else {
        $.get(url + "session/" + _session)
            .done(function (_response, stu, jqXHR) {

                if (jqXHR.status !== 200 || jqXHR.status !== 201) {
                    _doSessionInit();
                } else {
                    if (_response.status === "valid") {
                        _onSessionSuccess();
                    } else {
                        _doSessionInit();
                    }
                }

            });
    }

    function _doSessionInit() {
        $.post(url + "session/init", null, _onSessionSuccess);
    }

    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session", _session);
        _locationData();
    }

    function _locationData() {
        _linenotify();
        $(".change-line-notify").click(_changeLineNotify);
    };
    
    function _linenotify() {
        $.ajax(url + "linenotify/list/ASC/-1", {
            headers: {
                "Session": _session
            },
            dataType: "json",
            success: function (_response) {
                $(_response).each(function () {
                    var _this = this.data.lineNotifys[0],
                        group_name = _this.group_name,
                        line_token = _this.line_token
//                    $(".group-name").val(group_name)
//                    $(".line-token").val(line_token)
                });
            }
        });
    }
    
    function _changeLineNotify() {
        var groupName = $(".group-name").val(),
            lineToken = $(".line-token").val()
        $.ajax( url + "linenotify/1" , {
            method: "PUT",
            dataType:"JSON",
            headers: { "Session": _session },
            data: {
                    "group_name": groupName,
                    "line_token": lineToken
                },
            success: function (_response) {
                location.reload();
            }
        });
    }
    
}