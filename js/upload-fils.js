$(document).ready(init);

function init() {

    var url = window.api
    
    var _session = window.localStorage.getItem("session");
    if (!_session) {
        _doSessionInit();

    } else {
        $.get(url + "session/" + _session)
            .done(function (_response, stu, jqXHR) {

                if (jqXHR.status !== 200 || jqXHR.status !== 201) {
                    _doSessionInit();
                } else {
                    if (_response.status === "valid") {
                        _onSessionSuccess();
                    } else {
                        _doSessionInit();
                    }
                }

            });
    }

    function _doSessionInit() {
        $.post(url + "session/init", null, _onSessionSuccess);
    }

    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session", _session);
        _locationData()
    }

    function _locationData() {
        _webPageUpload();
        _machine();
        _material();
        _sizetype();
        _fileImgOnchange();
        $(".step-2 .send-btn").click(_order);
        $(".step-3 button").click(_upData);
        $(".up-step").change(eachField);
        $(".up-step").keyup(eachField);
    };
    
    function _webPageUpload() {
        
        $.ajax( url + "web_content/list/asc/-1/line_upload_file" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
                dataType:"json",
                success: function (_response) {
                    $(_response).each(function(){
                        var $this = this.data.web_contents,
                            step1 = $this[0].text,
                            step2 = $this[1].text,
                            step3 = $this[2].text
                        $(".upfile .step-box .s1 p").html(step1)
                        $(".upfile .step-box .s2 p").html(step2)
                        $(".upfile .step-box .s3 p").html(step3)
                        
                    });
                }
            });
        
    }


    function _machine() {

        $.ajax(url + "machine/list/ASC/-1", {
            headers: {
                "Session": _session
            },
            dataType: "json",
            success: function (_response) {
                $(_response).each(function () {
                    var _data = this.data.machines,
                        _length = _data.length
                    for (var _quantity = 0; _quantity < _length; _quantity++) {
                        var _thisData = _data[_quantity],
                            _location = _thisData.name,
                            _id = _thisData.id,
                            _newquantity = _quantity + 1
                        var _option = "<option value='" + _id + "'>" + _location + "</option>"
                        $(".location").append(_option)
                    }
                });
            }
        });
    };

    function _material() {

        $.ajax(url + "material/list/ASC/-1", {
            headers: {
                "Session": _session
            },
            dataType: "json",
            success: function (_response) {
                $(_response).each(function () {
                    var _data = this.data.materials,
                        _length = _data.length
                    for (var _quantity = 0; _quantity < _length; _quantity++) {
                        var _thisData = _data[_quantity],
                            _name = _thisData.name,
                            _price = _thisData.price,
                            _id = _thisData.id,
                            _newquantity = _quantity + 1
                        var _option = "<option value='" + _id + "'" + "price='" + _price + "'>" + _name + "</option>"

                        $(".output-color").append(_option)
                    }
                });

            }
        });
    };

    function _sizetype() {

        $.ajax(url + "sizetype/list/ASC/-1", {
            headers: {
                "Session": _session
            },
            dataType: "json",
            success: function (_response) {
                $(_response).each(function () {
                    var _data = this.data.sizetypes,
                        _length = _data.length
                    for (var _quantity = 0; _quantity < _length; _quantity++) {
                        var _thisData = _data[_quantity],
                            _name = _thisData.name,
                            _width = _thisData.width,
                            _height = _thisData.height,
                            _Wsize = _width / 10 + "cm",
                            _Hsize = _height / 10 + "cm",
                            _id = _thisData.id;
                        var _showSize = "( " + _Wsize + "x" + _Hsize + " )",
                            _newquantity = _quantity
                        if (_id == '99') {
                            var _option = "<option value='" + _id + "'" + "width='" + _width + "'" + "height='" + _height + "'>" + _name + "</option>"
                        } else {
                            var _option = "<option value='" + _id + "'" + "width='" + _width + "'" + "height='" + _height + "'>" + _name + " " + _showSize + "</option>"
                        }

                        $(".size").append(_option)
                    }
                });
//                var _forPrev = '<option class="opForPrev" style="display:none;" value="forPrev">同上</option>'
//                $(".size").append(_forPrev)
            }
        });
    };

    function _fileImgOnchange() {
        
        $(".step-1 input").change(function () {

            var file = this.files[0],
                _maxSize = file.size / 1000000 //MB
            
            if (_maxSize <= 700) {
                $(".file-size-error").hide();

                var _formData = new FormData();

                _formData.append("file", $(this)[0].files[0]);

                if ($(".step-1 input").val() != "") {
                    
                    var uploadApi = "//file.sky-fp.com/"
                    
                    $.ajax(uploadApi + "file/uploadtou", {
                        method: "POST",
                        contentType: false,
                        processData: false,
                        headers: {
                            "Session": _session
                        },
                        data: _formData,
                        xhr: function () {
                            var xhr = $.ajaxSettings.xhr();
                            if (onprogress && xhr.upload) {
                                xhr.upload.addEventListener("progress", onprogress, false);
                                $(".up-img-ready").show();
                                return xhr;
                            }
                        },
                        success: function (_response) {
                            var _file_id = _response.data.file_id
                            $(".step-1 input").attr("file_id", _file_id)
                            $('.progress').hide();
                            $(".up-img-fulfil").show();
                            var _ups = $(".up-step"),
                                _val = _ups.eq(0).val() != "" && _ups.eq(1).val() != "" && _ups.eq(2).val() != "no" && _ups.eq(3).val() != "no" && _ups.eq(7).val() != "no"

                            if (_val && (_ups.eq(4).val() != "no" && _ups.eq(4).val() != "99" && $(".step-1 input").attr("file_id") != "")) {
                                $(".send-btn").show();
                            } else {
                                $(".send-btn").hide()
                                if ((_ups.eq(4).val() == 99) && _ups.eq(5).val() && _ups.eq(6).val() && _val && $(".step-1 input").attr("file_id") != "") {
                                    $(".send-btn").show()
                                } else {
                                    $(".send-btn").hide()
                                }
                            };
                        },
                        error: function (jqXHR, exception) {
                            var msg = '';
                            if (jqXHR.status === 0) {
                                $(".disconnection").show()
                            } else if (jqXHR.status == 404) {
                                msg = 'Requested page not found. [404]';
                            } else if (jqXHR.status == 500) {
                                msg = 'Internal Server Error [500].';
                            } else if (exception === 'parsererror') {
                                msg = 'Requested JSON parse failed.';
                            } else if (exception === 'timeout') {
                                msg = 'Time out error.';
                            } else if (exception === 'abort') {
                                msg = 'Ajax request aborted.';
                            } else {
                                msg = 'Uncaught Error.\n' + jqXHR.responseText;
                            }
                        }
                    });
                } else {
                    $('.progress').hide();
                    return;
                }
            } else {
                $(".file-size-error").show();
                return;
            }


            //方法为上传的进度条
            function onprogress(evt) {

                var _ups = $(".up-step"),
                    _val = _ups.eq(0).val() != "" && _ups.eq(1).val() != "" && _ups.eq(2).val() != "no" && _ups.eq(3).val() != "no" && _ups.eq(7).val() != "no"

                var loaded = evt.loaded; //已经上传大小情况
                var tot = evt.total; //附件总大小
                var per = Math.floor(100 * loaded / tot) - 1; //已经上传的百分比
                
                if (per >= 0) {
                    $(".up-img-ready").hide();
                    $('.progress').show();
                }
                
                $("#progress").css("width", per + "%").find("span").html(per + "%");
            }
            
        });
    };




    var data = {
        "machine_id": "1",
        "note": "",
        "status": "enable",
        "payment": "cash",
        "phone": "1234",
        "is_pay": "N",
        "orders": [

                    ]
    }

    function _order() {

        if($(".up-step").eq(4).val() != "forPrev"){
            var _nowOption = $(".up-step").eq(4).val()
            window.typeId = $(".up-step").eq(4).val();  
        }else{
            _nowOption = window.typeId
        }
        /* ======= */
        var _nowSelect = $(".up-step").eq(4),
            _colorSelect = $(".up-step").eq(3),
            _size_types_id = _nowSelect.attr("typeId"),
            _colorNowOption = _colorSelect.val(),
            _colorPrice = _colorSelect.find("option").eq(_colorNowOption).attr("price")

        var _thisHeight = _nowSelect.find("option").eq(_nowOption).attr("height"),
            _thisWidth = _nowSelect.find("option").eq(_nowOption).attr("width"),
            _lastWidth = $(".data-list").last().find("span").eq(0).text(),
            _lastHeight = $(".data-list").last().find("span").eq(1).text(),
            _setWidth = $(".up-step").eq(5).val(),
            _setHeight = $(".up-step").eq(6).val()
        
         
        
        if ($(".up-step").eq(4).val() == "99") {
            var _upHeight = _setHeight * 10,
                _upWidth = _setWidth * 10
        } else {
            if ($(".up-step").eq(4).val() == "forPrev") {
                var _upHeight = _lastHeight * 10,
                    _upWidth = _lastWidth * 10
            } else {
                var _upHeight = _thisHeight,
                    _upWidth = _thisWidth
            }
        }

        var _width = _upWidth,
            _height = _upHeight

        if (_width < 600 && _height < 600) {

            if (_width < _height) {
                _height2 = 60;
                _width2 = Math.ceil(_width / 10);
            } else {
                _height2 = Math.ceil(_height / 10);
                _width2 = 60;
            }
        }
        if (_width >= 900 || _height >= 900) {
            if (_width < 900) {
                _height2 = Math.ceil(_height / 10);
                _width2 = 60;
            }
            if (_height < 900) {
                _width2 = Math.ceil(_width / 10);
                _height2 = 60;
            }
        }
        if (_width >= 900 && _height >= 900) {
            if (_width <= 1200 && _height <= 1200) {
                _width2 = 120;
                _height2 = 120;
            } else {
                _width2 = Math.ceil(_width);
                _height2 = Math.ceil(_height);
            }
        }

        if ($(".up-step").eq(3).val() == '1') {
            _width2 = Math.ceil(900 / 10);
            _height2 = Math.ceil(_height / 10);
        }

        if ($(".up-step").eq(4).val() == '4' && $(".up-step").eq(3).val() != '1') {
            _width2 = Math.ceil(_width / 10);
            _height2 = Math.ceil(_height / 10);
        }

        if (_width < 600 && _height < 900) {
            _width2 = Math.ceil(_upWidth / 10);
            _height2 = Math.ceil(_upHeight / 10);
        }


        var _paperSize1 = (_height2 * _width2),
            _paperSize = Math.ceil(_paperSize1 / 900),
            _price = _paperSize * _colorPrice,
            _sizeHtml = "<span>" + _upWidth / 10 + "</span>" + "cm " + "x " + "<span>" + _upHeight / 10 + "</span>" + "cm"

        if ($(".up-step").eq(4).val() == "99") {
            var _sizeName = "[ 自訂 ]",
                _sizeHtml = "<span>" + _upWidth / 10 + "</span>" + "cm " + "x " + "<span>" + _upHeight / 10 + "</span>" + "cm"
        } else {
            
            if($(".up-step").eq(4).val() == "forPrev"){
                var _sizeName = "[ 同上 ]",
                    _sizeHtml = "<span>" + _upWidth / 10 + "</span>" + "cm " + "x " + "<span>" + _upHeight / 10 + "</span>" + "cm"
            }else{
                var _sizeName = "[" + _nowSelect.find("option").eq(_nowOption).text().split(" ")[0] + "]"   
            }
        }

        var _file = $(".up-step").eq(0).val(),
            _arr = _file.split('\\'),
            _myDataName = _arr[_arr.length - 1].replace(/\s+/g, "_"),
            _phone = $(".up-step").eq(1).val(),
            _loc = $(".up-step").eq(7).val(),
            _locText = $(".up-step").eq(7).find("option").eq(_loc).text(),
            _material = $(".up-step").eq(2).val() - 1,
            _materialText = $(".up-step").eq(2).find("option").eq(_material).text(),
            _color = $(".up-step").eq(3).val(),
            _colorText = $(".up-step").eq(3).find("option").eq(_color).text(),
            _size = $(".up-step").eq(4).val(),
            _sizeText = $(".up-step").eq(4).find("option").eq(_size).text(),
            _sizeText2 = $(".up-step").eq(5).val() + "mm" + "x" + $(".up-step").eq(6).val() + "mm",
            _listHtml = "<div class='data-list'><div>" + _myDataName + "</div>" + "<div>" + _materialText + "-" + _colorText + "-" + _sizeName + "-" + _sizeHtml + "</div>" + "<div>" + "輸出費用 ： " + _price + " 元" + "</div></div>",
            _note = $(".file-note").val()




        $(".order").append(_listHtml)
        $(".user").text(_phone);
        $(".loc").text(_locText)
        if ($(".data-list").length > 4) {
            $(".newFile").hide();
        } else {
            $(".newFile").show();
        }
        $(".upload-list").show();
        $(".send-btn").hide()
        $(".up-step").find("option").show();
        $(".enter-size").hide();
        $(".up-step").eq(7).attr("disabled", "disabled")
        $(".up-step").eq(1).attr("readonly", "readonly")
        $(".up-step").eq(0).val("");
        $(".step-1 , .step-2").hide();
        $(".step-3").show();

        var _file_id = $(".step-1 input").attr("file_id")


        data.machine_id = _loc;
        data.phone = _phone;
        data.orders.push({
            "materials_id": _color,
            "size_types_id": _nowOption,
            "file_id": _file_id,
            "width": _width,
            "height": _height,
            "note": _note,
            "num": "1",
            "price": _colorPrice,
            "file_price_total": ""
        });

        $('.progress').hide();
        $(".up-img-fulfil").hide();
               
    };


    $(".inquiry-file button").click(inquiryFile);
    $(".pop-up .close").click(closePopup);
    $(".newFile button").click(newFile);

    function _upData() {

        $.ajax(url + "order/create_in", {
            method: "POST",
            dataType: "json",
            headers: {
                "Session": _session
            },
            data: data,
            success: function (_response) {}
        });

        $(".main").load("upload-fulfil.html")
    }
    
    
    function checklength(ctlid,maxlength){              
       if ($(".step-2 input").eq(0).val().length > maxlength) {
              $(".step-2 input").eq(0).val($(".step-2 input").eq(0).val().substring(0, maxlength));
              return false;
           }
    }
    
    $(function(){                
         $("#mycomments").keyup(function(){
              checklength("mycomments",100);
         });
    }); 
    

};


function inquiryFile() {
    $(".pop-up").addClass("open");
    $("body").css("overflow", "hidden");
};

function closePopup() {
    $(".pop-up").removeClass("open");
    $("body").css("overflow", "auto");
    $(".pop-up ul").eq(1).html("")
    $(".pop-up ul input").val("")
};

function eachField() {
    var _ups = $(".up-step"),
        _val = _ups.eq(0).val() != "" && _ups.eq(1).val() != "" && _ups.eq(2).val() != "no" && _ups.eq(3).val() != "no" && _ups.eq(7).val() != "no"

    if (_ups.eq(4).val() == "99") {
        $(".enter-size").show();
    } else {
        $(".enter-size").hide();
    };

    if (_val && (_ups.eq(4).val() != "no" && _ups.eq(4).val() != "99" && $(".step-1 input").attr("file_id") != "")) {
        $(".send-btn").show();
    } else {
        $(".send-btn").hide()
        if ((_ups.eq(4).val() == 99) && _ups.eq(5).val() && _ups.eq(6).val() && _val && $(".step-1 input").attr("file_id") != "") {
            $(".send-btn").show()
        } else {
            $(".send-btn").hide()
        }
    };
};


function newFile() {
    $(".step-1 , .step-2").show();
    $(".newFile").hide();
    $(".send-btn").hide();
    $(".step-1 input").attr("file_id", "")
    $(".step-2 input").eq(0).css("color","#999")
}

function ValidateNumber(e, pnumber) {
    if (!/^\d+$/.test(pnumber)) {
        $(e).val(/^\d+/.exec($(e).val()));
    }
    return false;
}