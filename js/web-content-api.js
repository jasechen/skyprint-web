$(document).ready(init);

function init() {
    var url = window.api
        
    var _session = window.localStorage.getItem("session");
    if ( !_session) {
        _doSessionInit();
    }else{
        $.get( url+"session/" + _session )
            .done(function (_response,stu,jqXHR) {

                if( jqXHR.status !== 200 || jqXHR.status !== 201 ){
                    _doSessionInit();
                }else{
                    if ( _response.status === "valid" ) {
                        _onSessionSuccess();
                    } else {
                        _doSessionInit();
                    }
                }

            });
    }
    
    function _doSessionInit() {
        $.post( url+"session/init", null, _onSessionSuccess);
    }
    
    function _onSessionSuccess(_response) {
        _session = _response.data.session
        window.localStorage.setItem("session",_session);
        _locationData();
    }
    
    function _locationData() {
        _webPageIndex();
        _webPageAbout();
        _webPageCampus();
        _webFaq();
    };
    
    
    function _webPageIndex() {
        
        $.ajax( url + "web_content/list/asc/-1/index" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                $(_response).each(function(){

                    var $this = this.data.web_contents,
                        nowCover = $this[0].fileLinks[0],
                        getDataType = nowCover.split(".").length-1,
                        dataType = nowCover.split(".")[getDataType],
                        article_1_title = $this[1].title,
                        article_1_subtitle = $this[1].subtitle,
                        article_1_text = $this[1].text,
                        article_1_img = $this[1].fileLinks[0],

                        article_2_title = $this[2].title,
                        article_2_subtitle = $this[2].subtitle,
                        article_2_text = $this[2].text,
                        article_2_img = $this[2].fileLinks[0],

                        coverSource = '<source src="'+ nowCover + '?v=' + (+new Date()) +'" type="video/mp4">',
                        coverImg = '<img src="'+nowCover+'">'


                    if(dataType == "mp4"){
                        $(".banner.index video").append(coverSource)
                    }else{
                        $(".banner.index").html("")
                        $(".banner.index").append(coverImg)
                    }

                    $(".article-1 h3").html(article_1_title)
                    $(".article-2 h3").html(article_2_title)
                    $(".article-1 div ul li h2").html(article_1_subtitle)
                    $(".article-2 div ul li h2").html(article_2_subtitle)
                    $(".article-1 div ul").append(article_1_text)
                    $(".article-2 div ul").append(article_2_text)
                    $(".article-1 div div img").attr("src",article_1_img)
                    $(".article-2 div div img").attr("src",article_2_img)

                });
            }
        });
    }
    
    
    function _webPageAbout() {
        
        $.ajax( url + "web_content/list/asc/-1/about" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                $(_response).each(function(){
                    var $this = this.data.web_contents,
                        nowCover = $this[0].fileLinks[0],
                        nowText = $this[1].text
                    $("article.about p").html(nowText)
                    $(".banner.about img").attr("src",nowCover)
                });
            }
        });
    }
    
    function _webPageCampus() {
        
        $.ajax( url + "web_content/list/asc/-1/school" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                $(_response).each(function(){
                    var $this = this.data.web_contents,
                        nowCover = $this[0].fileLinks[0],
                        nowText = $this[1].text
                    $("article.campus p").html(nowText)
                    $(".banner.campus img").attr("src",nowCover)
                });
            }
        });   
    }
    
    function _webFaq() {
        $.ajax( url + "web_content/list/DESC/-1/faq/content" + "?v=" + (+new Date()) , {
            headers: { "Session": _session },
            dataType:"json",
            success: function (_response) {
                var $this = _response.data.web_contents,
                    _length = $this.length
                for ( var _quantity = 0 ; _quantity < _length ; _quantity ++ ) {
                    var _html = "<li>"
                            +'<div class="question">'+ $this[_quantity].subtitle +"</div>"
                            +'<div class="answer">'+ $this[_quantity].text +'</div>'
                            + "</li>"
                    $("article.web-faq ul").append(_html)
                }
            }
        });
    }
  
}